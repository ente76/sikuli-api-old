package org.sikuli.api;

import org.sikuli.api.robot.desktop.DesktopScreen;

public class DesktopScreenRegion extends DefaultScreenRegion implements ScreenRegion {

	/**
	 * Creates a ScreenRegion in full screen on the default screen
	 */
	public DesktopScreenRegion(){
		super(new DesktopScreen(0));
	}

	/**
	 * Creates a ScreenRegion of the given x, y, width, height
	 * The screen will be determined depending on the center of the rectangle
         * provided. In case the center of the rectangle is outside of any
         * screen, the screen will be set to the default screen (0).
         *
	 * @param x	x
	 * @param y	y
	 * @param width	width
	 * @param height	height
	 */
	public DesktopScreenRegion(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

}
