package org.sikuli.api;

import org.sikuli.api.robot.desktop.DesktopInformation;
import org.sikuli.api.robot.desktop.DesktopScreen;

public class DefaultScreenLocation implements ScreenLocation {

	private int x = 0;
	private int y = 0;
	private Screen screen;

	public DefaultScreenLocation(Screen screenRef, int _x, int _y){
		setX(_x);
		setY(_y);
		screen = screenRef;
	}

        /*
         * Create a new screen location based on the coordinates. The screen
         * relating to those coordinates will be determined automatically and
         * defaults to screen 0 in case those coordinates are outside of all
         * screens.
         */
        public DefaultScreenLocation(int x, int y) {
                this.x=x;
                this.y=y;
                this.screen=DesktopInformation.getScreenAtCoord(x, y);
                if (this.screen==null) {
                    this.screen=new DesktopScreen(0);
                }
        }

	public DefaultScreenLocation(ScreenLocation loc){
		setX(loc.getX());
		setY(loc.getY());
		screen = loc.getScreen();
	}

	public ScreenLocation getRelativeScreenLocation(int xoffset, int yoffset){
		ScreenLocation loc =  new DefaultScreenLocation(screen, getX() + xoffset, getY() + yoffset);
		loc.setScreen(screen);
		return loc;
	}

	public String toString(){
		return "(" + getX() + "," + getY() + ")";
	}

	@Override
	public Screen getScreen() {
		return screen;
	}

	@Override
	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int y) {
		this.y = y;
	}
}
